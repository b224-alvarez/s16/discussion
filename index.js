// console.log("Hello World!");

let firstNumber = 12;
let secondNumber = 5;
let total = 0;

// ARITHMETIC OPERATORS
// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber;
console.log("Result of addition operator is " + total);

// Subtraction Operator - responsible for subtracting two or more numbers.
total = firstNumber - secondNumber;
console.log("Result of subtraction operator is " + total);

// Multiplication Operator - responsible for multiplying two or more numbers
total = firstNumber * secondNumber;
console.log("Result of multiplication operator is " + total);

// Division Operator - responsible for dividing two or more numbers
total = firstNumber / secondNumber;
console.log("Result of division operator is " + total);

// Modulo Operator - responsible for getting the remainder of two or more numbers in our division operation.
total = firstNumber % secondNumber;
console.log("Result of modulo operator is " + total);

// ASSIGNMENT OPERATORS
// Reassignment operator - should be a single equal, signifies re-assignment or new value into an exisiting variable.
total = 27;
console.log("Result of reassignment operator is: " + total);

// Addition Assignment Operator - uses the current value of the variable and ADDS a number to itself. Afterwards, reassigns it as the new value.
total += 3;
// total = total + 3;
console.log("Result of addition assignment operator is: " + total);

// Subtraction Assignment Operator - uses the current value of the variable and SUBTRACTS a number to itself. Afterwards, reassign it as the new value.
total -= 5;
// total = total - 5;
console.log("Result of subtraction assigment operator is: " + total);

// Multiplication Assignment Operator - uses the current value of the variable and MULTIPLIES a number to itself. Afterwards, reassign it as the new value.
total *= 4;
// total = total * 4;
console.log("Result of multplication assigment operator is: " + total);

// Division Assignment Operator - uses the current value of the variable and DIVIDES a number to itself. Afterwards, reassign it as the new value.
total /= 20;
// total = total / 20;
console.log("Result of division assigment operator is: " + total);

// MULTIPLE OPERATORS
let mdasTotal = 0;
let pemdasTotal = 0;

/* 
    When doing multiple operations, the program follows the MDAS rule. 
    MDAS (Multiplicaiton ,Division, Addition, Subtraction)
*/

mdasTotal = 2 + 1 - (5 * 4) / 1;
console.log("Result of multiple operators is " + mdasTotal);

/* 
    When doing multiple operations, the program follows the PEMDAS rule. 
    PEMDAS (Parenthesis, Exponent, Multiplicaiton ,Division, Addition, Subtraction)

    Exponents are declared by using double astersik ('**')
*/

pemdasTotal = 5 ** 2 + ((10 - 2) / 2) * 3;
console.log("Result of PEMDAS: " + pemdasTotal);

// INCREMENT AND DECREMENT OPERATORS
let incrementNumber = 1;
let decrementNumber = 5;

/* 
    Pre-Increment = adds 1 first before reading the value.
    Pre-Decrement = subtracts 1 first before reading the value.

    Post-Increment = reads the value first before adding 1.
    Post-Decrement = reads the value first before subtracting 1.
*/

let resultOfPreIncrement = ++incrementNumber;
let resultOfPreDecrement = --decrementNumber;

let resultofPostIncrement = incrementNumber++;
let resultOfPostDecrement = decrementNumber--;

console.log(resultOfPreIncrement);
console.log(resultOfPreDecrement);
console.log(resultofPostIncrement);
console.log(resultOfPostDecrement);

// COERCION
// Coercion - When you add a string value to a non-string value.
let a = "10";
let b = 10;
console.log(a + b);

// Non - Coercion - When you add 2 or more non string values.
let c = 10;
let d = 10;
console.log(c + d);

let e = "10";
let f = "10";
console.log(e + f);

// Typeof Keyword - returns the data type of a variable.
let stringType = "Hello";
let numberType = 1;
let booleanType = true;
let arrayType = ["1", "2", "3"];
let objectType = {
  objectKey: "Object Value",
};

console.log(typeof stringType);
console.log(typeof numberType);
console.log(typeof booleanType);
console.log(typeof arrayType);
console.log(typeof objectType);

// Computer reads "true" as 1
// Computer reads "false" as 0
console.log(true + 1);
console.log(false + 1);

// COMPARISON OPERATORS
// Equality Operator - checks if both values are the same; return true if it is or fals if otherwise.
console.log(5 == 5);
console.log("hello" == "hello");
console.log("2" == 2);

// Strict Equality Operator - Checks if both values AND data types are the same, returns true if it is.
console.log("2" === 2);
console.log(true === 1);

// Inequality Operator - checks if both values are NOT equal, returns true if they are not.
console.log(5 != 5);
console.log("hello" != "hello");
console.log("2" != 2);

// Strict Inequality Operator - checks if both values AND data types are not equal, returns true if they are not.
console.log(1 !== "1");
console.log(2 !== 2);

// RELATIONAL OPERATORS
let firstVar = 5;
let secondVar = 5;

// Greater than / GT operator - checks if the first value is greater than the second value.
console.log(firstVar > secondVar);

// Less than / LT operator - checks if the first value is less than the second value.
console.log(firstNumber < secondVar);

// Greater than or equal / GTE operator - checks if the first value is either greater than or equal to the second value.
console.log(firstVar >= secondVar);

// Less than or equal / LTE Operator - checks if the first value is either less than or equal to the second value.
console.log(firstVar <= secondVar);

// LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// AND Operator - returns "true" if both statments are true; returns "false" if one of the statements is ture.
console.log(isLegalAge && isRegistered);

// OR Operator - returns "true" if one of the statements is true; returns "false" if neither of the statements is true.
console.log(isLegalAge || isRegistered);

// NOT Operator - reverses the boolean value (from true to false, vice-versa)
console.log(!isLegalAge);

// Truthy and Falsy Values
// Everything that is either empty, or zero will equate to false, and everything that has some sort of value will equate to true.
console.log([] == false);
console.log("" == false);
// console.log(null == false);
console.log(0 == false);
